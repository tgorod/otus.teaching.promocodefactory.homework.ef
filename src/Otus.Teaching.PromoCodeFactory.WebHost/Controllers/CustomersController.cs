﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerService _service;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список всех клиентов
        /// </summary>
        /// <returns>сustomers</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var сustomers = await _service.GetAll();
            return Ok(_mapper.Map<List<CustomerShortResponse>>(сustomers));
        }

        /// <summary>
        /// Получить данные одного клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <returns>сustomer</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var сustomer = await _service.GetById(id);
            return Ok(_mapper.Map<Customer, CustomerResponse>(сustomer));
        }

        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <returns>результат создания</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            return Ok(await _service.Create(_mapper.Map<CreateOrEditCustomerRequest, Customer>(request)));
        }

        /// <summary>
        /// Обновление данных клиента вместе с его предпочтениями
        /// </summary>
        /// <returns>результат обновления</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var res = await _service.Update(id, _mapper.Map<CreateOrEditCustomerRequest, Customer>(request));
            if (res)
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns>результат удаления</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var res = await _service.Delete(id);
            if (res)
                return Ok();
            return NotFound();
        }
    }
}