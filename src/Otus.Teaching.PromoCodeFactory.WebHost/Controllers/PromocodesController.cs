﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IPromoCodeService _service;
        private IMapper _mapper;

        public PromocodesController(IPromoCodeService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Список промокодов.</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _service.GetAll();
            return Ok(_mapper.Map<List<PromoCodeShortResponse>>(promoCodes));
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromocodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promocode = _mapper.Map<GivePromoCodeRequest, PromoCode>(request);
            promocode.Preference = new Preference {
                Id = Guid.NewGuid(),
                Name = request.Preference
            };
            var promoCodeId = await _service.Create(promocode);
            return Ok(promoCodeId);
        }
    }
}