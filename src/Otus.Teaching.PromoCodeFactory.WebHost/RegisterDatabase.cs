﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class RegisterDatabase
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddDbContext<DatabaseContext>(options => options.UseSqlite(configuration.GetConnectionString("Sqlite")))
                .InstallRepositories()
                .InstallServices()
                ;
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
            .AddTransient<ICustomerService, CustomerService>()
            .AddTransient<IPreferenceService, PreferenceService>()
            .AddTransient<IPromoCodeService, PromoCodeService>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<ICustomerRepository, CustomerRepository>()
                .AddTransient<IPreferenceRepository, PreferenceRepository>()
                .AddTransient<IPromoCodeRepository, PromoCodeRepository>()
                .AddTransient<IEmployeeRepository, EmployeeRepository>()
                .AddTransient<IRoleRepository, RoleRepository>();
            return serviceCollection;
        }
    }
}
