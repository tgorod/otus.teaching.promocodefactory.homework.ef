﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<Guid, Preference>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src))
                .ForMember(d => d.Name, map => map.Ignore());


            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.PromoCodes, map => map.Ignore())
                .ForMember(d => d.Preferences, map => map.MapFrom(src => src.PreferenceIds));

            CreateMap<Customer, CustomerResponse>();

            CreateMap<Customer, CustomerShortResponse>();
        }
    }
}
