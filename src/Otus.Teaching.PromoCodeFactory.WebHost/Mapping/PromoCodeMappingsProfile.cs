﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PromoCodeMappingsProfile : Profile
    {
        public PromoCodeMappingsProfile()
        {
            CreateMap<PromoCode, PromoCodeShortResponse>();

            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.BeginDate, map => map.MapFrom(src => DateTime.Now))
                .ForMember(d => d.EndDate, map => map.MapFrom(src => DateTime.Now.AddYears(1)))
                .ForMember(d => d.Code, map => map.MapFrom(src => src.PromoCode))
                .ForMember(d => d.PartnerManagerId, map => map.Ignore())
                .ForMember(d => d.PartnerManager, map => map.Ignore())
                .ForMember(d => d.PreferenceId, map => map.Ignore())
                .ForMember(d => d.Preference, map => map.Ignore());
        }
    }
}
