﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            },
            new Preference
            {
                Id = Guid.Parse("1751BB01-9341-47C6-8995-9F1E2E49FEE2"),
                Name = "Вязание",
            }
       };

        public static IEnumerable<Customer> Customers = new List<Customer>()
        {
            new Customer()
        {
            Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
            }
    };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
        new PromoCode()
        {
            Id = Guid.Parse("E3505983-F8DA-43DD-A82E-1F1F9AFF645F"),
            Code = "ASD",
            PartnerManagerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
            PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
        },
            new PromoCode()
            {
                Id = Guid.Parse("CFEE615F-8524-4CAA-AD3B-A794C119A50D"),
                Code = "Birth",
                PartnerManagerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
            },
            new PromoCode()
            {
                Id = Guid.Parse("1B28AA50-3275-4D3E-AD2A-CE6962FFE994"),
                Code = "NewYear",
                PartnerManagerId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PreferenceId = Guid.Parse("1751BB01-9341-47C6-8995-9F1E2E49FEE2"),
            }
        };

        public static IEnumerable<(Guid CustomerId, Guid PreferenceId)> CustomerPreference = new List<(Guid CustomerId, Guid PreferenceId)>()
        {
            ( Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), Guid.Parse("1751BB01-9341-47C6-8995-9F1E2E49FEE2") ),
            ( Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") )
        };

    }
}