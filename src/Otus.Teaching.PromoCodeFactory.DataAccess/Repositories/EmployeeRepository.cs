﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : EfRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <returns>клиента</returns>
        public override async Task<Employee> GetByIdAsync(Guid id)
        {
            var query = Context.Set<Employee>().AsQueryable();
            query = query
                .Include(c => c.Role)
                .Where<Employee>(c => c.Id == id);

            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// Получить сущность по имени
        /// </summary>
        /// <param name="name">имя клиента</param>
        /// <returns>клиента</returns>
        public async Task<Employee> GetByName(string name)
        {
            var query = Context.Set<Employee>().AsQueryable()
                .Where(p => p.FirstName.StartsWith(name)
                         || p.LastName.StartsWith(name));

            return await query.SingleOrDefaultAsync();
        }

        // этот метод не используется
        public override Task<bool> UpdateAsync(Employee entity)
        {
            return Task.FromResult(true);
        }
    }
}
