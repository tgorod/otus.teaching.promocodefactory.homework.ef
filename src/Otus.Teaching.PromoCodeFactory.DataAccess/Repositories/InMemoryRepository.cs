﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public bool Delete(Guid id)
        {
            var obj = Data.FirstOrDefault(x => x.Id == id);
            if (obj == null)
            {
                return false;
            }
            Data.ToList().Remove(obj);
            return true;
        }

        public Task<bool> UpdateAsync(T entity)
        {
            var obj = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (obj == null)
            {
                return Task.FromResult(false);
            }
            // В этом проекте InMemoryRepository не нужен, поэтому пусть будет так
            Data.ToList().Remove(obj);
            Data.ToList().Add(entity);
            return Task.FromResult(true);
        }

        public Task<T> AddAsync(T entity)
        {
            Data.ToList().Add(entity);
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == entity.Id));
        }

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        public virtual Task SaveChangesAsync()
        {
            return Task.FromResult(true);
        }
    }
}