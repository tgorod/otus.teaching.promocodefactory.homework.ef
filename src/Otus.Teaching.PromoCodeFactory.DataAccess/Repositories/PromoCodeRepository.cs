﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository : EfRepository<PromoCode>, IPromoCodeRepository
    {
        public PromoCodeRepository(DatabaseContext context) : base(context)
        {
        }


        // этот метод не используется
        public override Task<bool> UpdateAsync(PromoCode entity)
        {
            return Task.FromResult(true);
        }
    }
}
