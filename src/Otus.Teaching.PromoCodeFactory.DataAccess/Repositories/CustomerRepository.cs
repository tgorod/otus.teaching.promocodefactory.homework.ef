﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<bool> UpdateAsync(Customer entity)
        {
            if (entity == null)
            {
                return false;
            }
            var obj = await GetByIdAsync(entity.Id);
            if (obj == null)
            {
                return false;
            }

            obj.FirstName = entity.FirstName;
            obj.LastName = entity.LastName;
            obj.Email = entity.Email;


            if (obj.Preferences != null)
                obj.Preferences.Clear();
            else
                obj.Preferences = new List<Preference>();

            var query = Context.Set<Preference>().AsQueryable();
            foreach(Guid id in entity.Preferences.Select(p => p.Id))
            {
                var pref = query
                    .Where(p => p.Id == id)
                    .SingleOrDefault();
                if (pref != null)
                    obj.Preferences.Add(pref);
            }


            return true;
        }

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <returns>клиента</returns>
        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var query = Context.Set<Customer>().AsQueryable();
            query = query
                .Include(c => c.PromoCodes)
                .Include(c => c.Preferences)
                .Where(c => c.Id == id);

            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// Получить промокоды
        /// </summary>
        /// <returns>промокоды</returns>
        public async Task<IEnumerable<PromoCode>> GetPromoCodes()
        {
            var query = Context.Set<Customer>()
                .AsQueryable()
                .Include(c => c.PromoCodes)
                .SelectMany(c => c.PromoCodes);

            return await query.ToListAsync();
        }

        /// <summary>
        /// Получить сущности по предпочтению
        /// </summary>
        /// <param name="preference">предпочтение</param>
        /// <returns>клиенты</returns>
        public async Task<IEnumerable<Customer>> GetByPreference(string preference)
        {
            var query = Context.Set<Customer>().AsQueryable()
                .Include(c => c.Preferences)
                .Where(p => p.Preferences.Any(pr => pr.Name == preference));

            return await query.ToListAsync();
        }
    }
}
