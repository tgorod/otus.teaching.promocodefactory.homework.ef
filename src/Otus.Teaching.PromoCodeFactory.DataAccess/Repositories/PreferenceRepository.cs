﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<bool> UpdateAsync(Preference entity)
        {
            if (entity == null)
            {
                return false;
            }
            var obj = await GetByIdAsync(entity.Id);
            if (obj == null)
            {
                return false;
            }
            obj.Name = entity.Name;
            return true;
        }
    }
}
