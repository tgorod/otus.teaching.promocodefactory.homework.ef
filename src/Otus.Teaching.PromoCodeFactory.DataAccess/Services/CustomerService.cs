﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        //private readonly IPreferenceRepository _preferenceRepository;

        public CustomerService(
            ICustomerRepository customerRepository//,
            //IPreferenceRepository preferenceRepository
            )
        {
            _customerRepository = customerRepository;
            //_preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить весь список.
        /// </summary>
        /// <returns> Список клиентов. </returns>
        public async Task<IEnumerable<Customer>> GetAll()
        {
            return await _customerRepository.GetAllAsync();
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        public async Task<Customer> GetById(Guid id)
        {
            return await _customerRepository.GetByIdAsync(id);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="customer">клиент</para
        public async Task<Guid> Create(Customer customer)
        {
            var res = await _customerRepository.AddAsync(customer);
            await _customerRepository.SaveChangesAsync();
            return res.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="customer">клиент</param>
        public async Task<bool> Update(Guid id, Customer customer)
        {
            customer.Id = id;
            var res = await _customerRepository.UpdateAsync(customer);
            if (res)
                await _customerRepository.SaveChangesAsync();
            return res;
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        public async Task<bool> Delete(Guid id)
        {
            var res = _customerRepository.Delete(id);
            if (res)
                await _customerRepository.SaveChangesAsync();
            return res;
        }

        /// <summary>
        /// Получить список промокодов.
        /// </summary>
        /// <returns> Список промокодов. </returns>
        public async Task<IEnumerable<PromoCode>> GetAllPromoCodes()
        {
            return await _customerRepository.GetPromoCodes();
        }
    }
}
