﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public PromoCodeService(
            IPromoCodeRepository promoCodeRepository,
            ICustomerRepository customerRepository,
            IEmployeeRepository employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить весь список.
        /// </summary>
        /// <returns> Список промокодов. </returns>
        public async Task<IEnumerable<PromoCode>> GetAll()
        {
            return await _promoCodeRepository.GetAllAsync();
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="promoCode">промокод</param>
        public async Task<Guid> Create(PromoCode promoCode)
        {
            // ищем партнера
            promoCode.PartnerManager = await _employeeRepository.GetByName(promoCode.PartnerName);
            if (promoCode.PartnerManager == null)
                return Guid.Empty;

            var res = await _promoCodeRepository.AddAsync(promoCode);
            await _promoCodeRepository.SaveChangesAsync();

            // ищем клиентов с переданным предпочтением и добавлять им данный промокод
            var customers = await _customerRepository.GetByPreference(promoCode.Preference.Name);
            if (customers?.Any() ?? false)
            {
                foreach (var customer in customers)
                {
                    if (customer.PromoCodes == null)
                        customer.PromoCodes = new List<PromoCode>();
                    customer.PromoCodes.Add(res);
                }

                await _customerRepository.SaveChangesAsync();
            }
            return res.Id;
        }
    }
}
