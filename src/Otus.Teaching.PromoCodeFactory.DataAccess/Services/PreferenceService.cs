﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IPreferenceRepository _preferenceRepository;

        public PreferenceService(
            IPreferenceRepository preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить весь список.
        /// </summary>
        /// <returns> Список предпочтений. </returns>
        public async Task<IEnumerable<Preference>> GetAll()
        {
            return await _preferenceRepository.GetAllAsync();
        }
    }
}
