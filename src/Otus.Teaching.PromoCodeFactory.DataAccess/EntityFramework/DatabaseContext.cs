﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    /// <summary>
    /// Контекст
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            // сделано 2 миграции, больше не пересоздаем БД при каждом вызове контроллера
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        /// <summary>
        /// Сотрудники
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Роли сотрудников
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Клиенты
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public DbSet<Preference> Preferences { get; set; }

        /// <summary>
        /// Промокоды
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>()
                .HasOne(u => u.Role)
                .WithOne()
                ;

            modelBuilder.Entity<PromoCode>()
                .HasOne(c => c.PartnerManager)
                .WithMany()
                .HasForeignKey("PartnerManagerId")
                ;

            /// MANY TO MANY   Customer --- Preference
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany()
                .UsingEntity<Dictionary<string, object>>(
                    "CustomerPreference",
                    r => r.HasOne<Preference>().WithMany().HasForeignKey("PreferenceId"),
                    l => l.HasOne<Customer>().WithMany().HasForeignKey("CustomerId"),
                    je =>
                    {
                        je.HasKey("CustomerId", "PreferenceId");
                        je.HasData(
                            new { CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") },
                            new { CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("1751BB01-9341-47C6-8995-9F1E2E49FEE2") });
                    });

            // заменим ее на такую: 
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasKey(cp => new { cp.PreferenceId, cp.CustomerId });
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(cp => cp.Preference)
            //    .WithMany()
            //    .HasForeignKey(bc => bc.PreferenceId);
            //modelBuilder.Entity<CustomerPreference>()
            //    .HasOne(cp => cp.Customer)
            //    .WithMany()
            //    .HasForeignKey(bc => bc.CustomerId);



            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            // пусть это будет настроено через DataAnnotations 
            //modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            //modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            //modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(20);
            modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(256);
            modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(100);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }

    }
}
