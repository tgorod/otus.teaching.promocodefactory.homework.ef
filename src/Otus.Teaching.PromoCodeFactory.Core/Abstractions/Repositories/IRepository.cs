﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns>сущность</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">ID удалённой сущности</param>
        /// <returns>была ли сущность удалена</returns>
        bool Delete(Guid id);

        /// <summary>
        /// Изменить сущность
        /// </summary>
        /// <param name="entity">измененная сущность</param>
        Task<bool> UpdateAsync(T entity);

        /// <summary>
        /// Добавить в базу одну сущность
        /// </summary>
        /// <param name="entity">сущность для добавления</param>
        /// <returns>добавленная сущность</returns>
        Task<T> AddAsync(T entity);

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        Task SaveChangesAsync();
    }
}