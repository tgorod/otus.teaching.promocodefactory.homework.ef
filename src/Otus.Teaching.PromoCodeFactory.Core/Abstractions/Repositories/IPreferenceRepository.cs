﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Репозиторий работы с предпочтениями
    /// </summary>
    public interface IPreferenceRepository : IRepository<Preference>
    {
    }
}
