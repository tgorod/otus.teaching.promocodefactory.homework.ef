﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPromoCodeService
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список промокодов. </returns>
        Task<IEnumerable<PromoCode>> GetAll();

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="promoCode">промокод</param>
        Task<Guid> Create(PromoCode promoCode);
    }
}
