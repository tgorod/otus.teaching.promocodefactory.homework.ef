﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Репозиторий работы с клиентами
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<IEnumerable<PromoCode>> GetPromoCodes();

        Task<IEnumerable<Customer>> GetByPreference(string preference);
    }
}
