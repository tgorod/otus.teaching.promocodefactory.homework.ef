﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<Employee> GetByName(string name);
    }
}
