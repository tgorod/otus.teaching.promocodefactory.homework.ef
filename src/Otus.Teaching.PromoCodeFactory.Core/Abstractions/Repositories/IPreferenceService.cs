﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPreferenceService
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список предпочтений. </returns>
        Task<IEnumerable<Preference>> GetAll();
    }
}
