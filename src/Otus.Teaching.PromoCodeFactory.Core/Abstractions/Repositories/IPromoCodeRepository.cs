﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Репозиторий работы с промокодами
    /// </summary>
    public interface IPromoCodeRepository : IRepository<PromoCode>
    {
    }
}
