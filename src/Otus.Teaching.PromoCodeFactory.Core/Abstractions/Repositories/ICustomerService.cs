﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerService
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <returns> Список клиентов. </returns>
        Task<IEnumerable<Customer>> GetAll();

        /// <summary>
        /// Получить список промокодов.
        /// </summary>
        /// <returns> Список промокодов. </returns>
        Task<IEnumerable<PromoCode>> GetAllPromoCodes();

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>клиент</returns>
        Task<Customer> GetById(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="customer">клиент</para
        Task<Guid> Create(Customer customer);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="customer">клиент</param>
        Task<bool> Update(Guid id, Customer customer);

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task<bool> Delete(Guid id);
    }
}
