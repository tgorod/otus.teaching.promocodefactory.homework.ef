﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }
        public Guid PreferenceId { get; set; }

        [ForeignKey("PreferenceId")]
        public Preference Preference { get; set; }
    }
}
